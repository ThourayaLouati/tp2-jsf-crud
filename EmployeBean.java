/*Ajouter ce managedBean*/

@ManagedBean(name = "employeBean") 
@SessionScoped
public class EmployeBean implements Serializable {

private static final long serialVersionUID = 1L;

private String login; private String password;  private String email; 
private Boolean isActif;  private Role role; 
@EJB
EmployeService employeService; 

public void addEmploye() {
employeService.ajouterEmploye(new Employe(login,  password, email, isActif, role)); }  
 // getters & setters
}
